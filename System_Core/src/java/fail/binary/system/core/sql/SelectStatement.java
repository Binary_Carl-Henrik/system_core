package fail.binary.system.core.sql;

import fail.binary.system.rep.core.util.Env;
import net.comactivity.core.data.Environment;
import net.comactivity.core.repository.RepositoryUtil;
import net.comactivity.core.repository.Table;

public class SelectStatement extends net.comactivity.core.sql.SelectStatement {

	public SelectStatement(String metadataId, String tableName) {
		//TODO MIGRATE TO OTHER LIB
		super(getTable(metadataId, tableName), env());
	}
	
	@Override
	public SqlTable addImportedTable(String joinKey) {
		
		net.comactivity.core.sql.SqlTable table = super.addImportedTable(joinKey);
		
		return new SqlTable(table);
	}
	
	private final static Table getTable(String metadataId, String tableName) {
		return RepositoryUtil.getTable(metadataId, tableName);
	}
	
	protected static Environment env() {
		return Env.get();
	}
}