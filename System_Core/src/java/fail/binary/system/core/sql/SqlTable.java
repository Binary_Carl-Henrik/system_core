package fail.binary.system.core.sql;

public class SqlTable extends net.comactivity.core.sql.SqlTable {

	//TODO MIGRATE TO ANOTHER Select library
	public SqlTable(net.comactivity.core.sql.SqlTable table) {
		super(table.getSqlStatement(), table.getTable(), table.getAliasName(), table.getConstraint(), table.getJoinType());
	}

}
