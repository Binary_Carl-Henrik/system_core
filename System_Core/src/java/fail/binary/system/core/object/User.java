package fail.binary.system.core.object;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import fail.binary.system.core.sql.SelectStatement;
import fail.binary.system.core.util.IDGenerator;
import fail.binary.system.rep.core.impl.UserGroupRecord;
import fail.binary.system.rep.core.impl.UserRecord;

public class User extends AbstractRecordObject<UserRecord> {
	
	/* ------------------------------------------------------------
	 *  Constants & Variables
	 * ------------------------------------------------------------
	 */
	private Map<Long, UserGroup> _userGroups = new HashMap<Long, UserGroup>();
	
	/* ------------------------------------------------------------
	 *  Initialize
	 * ------------------------------------------------------------
	 */
	public static User create(String userName) {
		
		UserRecord userRecord = UserRecord.create(0l);
		userRecord.setUserName(userName);
		
		User user = new User(userRecord);
		user.setIsStored(false);
		
		return user;
	}
	
	public User(UserRecord userRecord) {
		super(userRecord);
	}
	
	/* ------------------------------------------------------------
	 *  Id
	 * ------------------------------------------------------------
	 */
	public long getId() {
		return getRecord().getUserId();
	}
	
	public String getUsername() {
		return getRecord().getUserName();
	}
	
	/* ------------------------------------------------------------
	 *  Authentication
	 * ------------------------------------------------------------
	 */
	public void setPassword(String passwordHash) {
		getRecord().setPasswordHash(passwordHash);
	}
	
	public String getEncryptedPassword() {
		return getRecord().getPasswordHash();
	}
	
	public void setSecretId(String secretId) {
		//TODO SECRET MANAGE
	}
	
	public String getSecretId() {
		//TODO SECRET MANAGE
		return "";
	}
	
	public String getSalt() {
		return getRecord().getPasswordSalt();
	}
	
	public void setSalt(String salt) {
		getRecord().setPasswordSalt(salt);
	}
	
	/* ------------------------------------------------------------
	 *  Save
	 * ------------------------------------------------------------
	 */
	public void executeSave() throws SQLException {
		
		if(!isStored()) {
			getRecord().setUserId(IDGenerator.generate());
		}
		
		getRecord().acceptChanges();
	}
	
	/* ------------------------------------------------------------
	 *  Group
	 * ------------------------------------------------------------
	 */
	public boolean hasAccess(Group group) throws Exception {
		return getUserGroup(group) != null;
	}
	
	public UserGroup getUserGroup(Group group) throws Exception {
		
		if(_userGroups.isEmpty()) {
			loadUserGroups();
		}
		
		return _userGroups.get(group.getId());
	}
	
	private void loadUserGroups() throws Exception {
		
		SelectStatement selectStatement = new SelectStatement(UserGroupRecord.METADATAID, UserGroupRecord.TABLENAME);
		selectStatement.addAllSelectColumns();
		
		selectStatement.addWhere(UserGroupRecord.USERID, getId());
		
		List<UserGroupRecord> records = UserGroupRecord.listRecords(selectStatement);
		
		for(UserGroupRecord record : records) {
			_userGroups.put(record.getGroupId(), new UserGroup(record));
		}
	}
}