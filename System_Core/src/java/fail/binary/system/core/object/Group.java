package fail.binary.system.core.object;

import fail.binary.system.core.util.IDGenerator;
import fail.binary.system.rep.core.impl.GroupRecord;

public class Group extends AbstractRecordObject<GroupRecord> {

	/* ------------------------------------------------------------
	 *  Constants & Variables
	 * ------------------------------------------------------------
	 */
	//TODO MOVE?
	public static enum TYPE {
		PUBLIC(0), SHARED(1), PRIVATE(2);
		
		private final int _value;
		
		private static TYPE get(int type) {
			
			if(PUBLIC.value() == type) {
				return PUBLIC;
			} else if(SHARED.value() == type) {
				return SHARED;
			} else if(PRIVATE.value() == type) {
				return PRIVATE;
			}
			
			return null;
		}
		
		private TYPE(int value) {
			_value = value;
		}
		
		public int value() {
			return _value; 
		}
	}
	
	//TODO MOVE?
	public static enum STATUS {
		ACTIVE(1), LOCKED(2), INACTIVE(9);
		
		private final int _value;
		
		private static STATUS get(int status) {
			
			if(ACTIVE.value() == status) {
				return ACTIVE;
			} else if(LOCKED.value() == status) {
				return LOCKED;
			} else if(INACTIVE.value() == status) {
				return INACTIVE;
			}
			
			return null;
		}
		
		private STATUS(int value) {
			_value = value;
		}
		
		public int value() {
			return _value; 
		}
	}
	
	
	/* ------------------------------------------------------------
	 *  Initialize
	 * ------------------------------------------------------------
	 */
	public static final Group create(String name, TYPE type) {
		
		GroupRecord groupRecord = GroupRecord.create(IDGenerator.generate());
		
		groupRecord.setName(name);
		
		Group group = new Group(groupRecord);
		group.setIsStored(false);
		group.setType(type);
		
		return group;
	}
	
	public Group(GroupRecord record) {
		super(record);
		
	}
	
	/* ------------------------------------------------------------
	 *  Id
	 * ------------------------------------------------------------
	 */
	public long getId() {
		return getRecord().getGroupId();
	}
	
	public String getName() {
		return getRecord().getName();
	}
	
	/* ------------------------------------------------------------
	 *  Type
	 * ------------------------------------------------------------
	 */
	private void setType(TYPE type) {
		getRecord().setType(type.value());
	}
	
	public TYPE getType() {
		return TYPE.get(getRecord().getType());
	}
	
	/* ------------------------------------------------------------
	 *  Status
	 * ------------------------------------------------------------
	 */
	public void setStatus(STATUS status) {
		getRecord().setStatus(status.value());
	}
	
	public STATUS getStatus() {
		return STATUS.get(getRecord().getStatus());
	}

	/* ------------------------------------------------------------
	 *  Save
	 * ------------------------------------------------------------
	 */
	@Override
	protected void executeSave() throws Exception {
		
		getRecord().acceptChanges();
	}
}