package fail.binary.system.core.object;

abstract public class AbstractRecordObject<T> {
	
	/* ------------------------------------------------------------
	 *  Constants & Variables
	 * ------------------------------------------------------------
	 */
	private final T _record;
	private boolean _isStored = true;
	
	
	/* ------------------------------------------------------------
	 *  Initialize
	 * ------------------------------------------------------------
	 */
	public AbstractRecordObject(T record) {
		_record = record;
	}
	
	
	/* ------------------------------------------------------------
	 *  Save
	 * ------------------------------------------------------------
	 */
	/**
	 * Stores the data to the data source
	 * @throws Exception
	 */
	public void save() throws Exception {
		
		executeSave();
		
		setIsStored(true);
	}
	
	abstract protected void executeSave() throws Exception;
	
	
	/* ------------------------------------------------------------
	 *  Record
	 * ------------------------------------------------------------
	 */
	protected T getRecord() {
		return _record;
	}
	
	
	/* ------------------------------------------------------------
	 *  Utilities
	 * ------------------------------------------------------------
	 */
	protected void setIsStored(boolean isStored) {
		_isStored = isStored;
	}
	
	protected boolean isStored() {
		return _isStored;
	}
}