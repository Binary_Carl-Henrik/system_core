package fail.binary.system.core.object;

import fail.binary.system.rep.core.impl.UserGroupRecord;

public class UserGroup extends AbstractRecordObject<UserGroupRecord> {
	
	/* ------------------------------------------------------------
	 *  Constants & Variables
	 * ------------------------------------------------------------
	 */
	private Group _group;
	
	/* ------------------------------------------------------------
	 *  Initialize
	 * ------------------------------------------------------------
	 */
	public UserGroup(UserGroupRecord record) {
		super(record);
	}
	
	/* ------------------------------------------------------------
	 *  Access
	 * ------------------------------------------------------------
	 */
	public void setCanRead(boolean canRead) {
		getRecord().setCanRead(canRead);
	}
	
	public void setCanEdit(boolean canEdit) {
		getRecord().setCanEdit(canEdit);
	}
	
	public void setCanCreate(boolean canCreate) {
		getRecord().setCanCreate(canCreate);
	}
	
	public void setCanDelete(boolean canDelete) {
		getRecord().setCanDelete(canDelete);
	}

	/* ------------------------------------------------------------
	 *  Save
	 * ------------------------------------------------------------
	 */
	@Override
	protected void executeSave() throws Exception {
		getRecord().acceptChanges();
	}
}