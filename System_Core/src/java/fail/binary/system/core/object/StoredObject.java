package fail.binary.system.core.object;

import java.io.Serializable;

public interface StoredObject extends Serializable {
	
	public String getStorageId();
	
	public String getStorageType();
	
	public String getStorageLocation();

}