package fail.binary.system.core.object;

import java.util.Date;

import fail.binary.system.core.storage.FileStorage;
import fail.binary.system.core.util.Config;

public class Session implements StoredObject {

	/* ------------------------------------------------------------
	 *  Constants & Variables
	 * ------------------------------------------------------------
	 */
	/**
	 * v. 1.0.0
	 */
	private static final long serialVersionUID = -1552325145645037366L;
	
	private final String _sessionId;
	private final User _user;
	private final Date _createdDate;
	private Date _lastUsedDate;
	
	/* ------------------------------------------------------------
	 *  Initialize
	 * ------------------------------------------------------------
	 */
	public Session(String sessionId, User user) {
		_sessionId = sessionId;
		_user = user;
		_createdDate = new Date();
	}
	
	/* ------------------------------------------------------------
	 *  Session
	 * ------------------------------------------------------------
	 */
	public String getSessionId() {
		return _sessionId;
	}
	
	public Date getDateCreated() {
		return _createdDate;
	}
	
	public Date getLastUsed() {
		return _lastUsedDate;
	}
	
	public void updateLastUsed() {
		_lastUsedDate = new Date();
	}
	
	/* ------------------------------------------------------------
	 *  User
	 * ------------------------------------------------------------
	 */
	public User getUser() {
		return _user;
	}
	
	/* ------------------------------------------------------------
	 *  Utilities
	 * ------------------------------------------------------------
	 */
	@Override
	public boolean equals(Object obj) {
		
		if(obj == null) {
			return false;
		}
		
		if(obj instanceof Session) {
			if(_sessionId.contentEquals(((Session) obj)._sessionId)) {
				return true;
			}
		}
		
		return false;
	}

	@Override
	public String getStorageId() {
		return getSessionId() + ".session";
	}

	@Override
	public String getStorageType() {
		return FileStorage.STORAGE_TYPE;
	}

	@Override
	public String getStorageLocation() {
		return Config.getSessionStorageLocation();
	}
}