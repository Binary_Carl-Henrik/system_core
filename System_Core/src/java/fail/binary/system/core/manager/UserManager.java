package fail.binary.system.core.manager;

import java.util.ArrayList;
import java.util.List;

import fail.binary.system.core.exception.SystemError;
import fail.binary.system.core.exception.UserError;
import fail.binary.system.core.exception.ValidationException;
import fail.binary.system.core.object.Group;
import fail.binary.system.core.object.User;
import fail.binary.system.core.object.UserGroup;
import fail.binary.system.core.sql.SelectStatement;
import fail.binary.system.core.sql.SqlTable;
import fail.binary.system.core.util.EncryptionUtil;
import fail.binary.system.core.util.StringUtil;
import fail.binary.system.rep.core.impl.UserGroupRecord;
import fail.binary.system.rep.core.impl.UserRecord;

public class UserManager extends AbstractManager {
	
	/* ------------------------------------------------------------
	 *  Constants & Variables
	 * ------------------------------------------------------------
	 */
	private final static String JOIN_KEY_USER_USERGROUP = "UserGroups";
	
	/* ------------------------------------------------------------
	 *  User
	 * ------------------------------------------------------------
	 */
	public final static User createUser(String username, String unencryptedPassword) throws Exception {
		
		validateCreateUser(username, unencryptedPassword);
		
		String secret = EncryptionUtil.generateSecret();
		String salt = EncryptionUtil.generateSalt();
		
		String passwordHash = EncryptionUtil.generateEncryptedPassword(salt, unencryptedPassword, secret);
		
		User user = User.create(username);
		user.setPassword(passwordHash);
		user.setSalt(salt);
		
		//TODO SAVE SECRET BUT WHERE? EXTERNAL SERVICE?
		//user.setSecretId(secret);
		user.save();
		
		//Personal user group
		Group group = createGroup(username, Group.TYPE.PRIVATE);
		
		UserGroup userGroup = createUserGroup(user, group, false);
		userGroup.setCanEdit(true);
		userGroup.setCanCreate(true);
		userGroup.setCanDelete(true);
		
		userGroup.save();
		
		return user;
	}
	
	/**
	 * Finds the user of the user name
	 * @param username
	 * @return
	 * @throws Exception
	 */
	public final static User getUser(String username) throws Exception {
		
		SelectStatement selectStatement = new SelectStatement(UserRecord.METADATAID, UserRecord.TABLENAME);
		
		selectStatement.addAllSelectColumns();
		
		selectStatement.addWhere(UserRecord.USERNAME, username);
		
		List<UserRecord> records = UserRecord.listRecords(selectStatement);
		
		if(records.isEmpty()) {
			return null;
		}
		
		return new User(records.get(0));
	}
	
	private static void validateCreateUser(String username, String unencryptedPassword) throws Exception {
		
		List<SystemError> errors = new ArrayList<SystemError>();
		
		//TODO PASSWORD RULES?
		if(!StringUtil.isDefined(unencryptedPassword)) {
			errors.add(new UserError(UserError.PASSWORD_INVALID, "Password is not set"));
		}
		
		if(getUser(username) != null) {
			errors.add(new UserError(UserError.USERNAME_OCCUPIED, "The username is occupied"));
		}
		
		if(!errors.isEmpty()) {
			throw new ValidationException(errors);
		}
	}
	
	/* ------------------------------------------------------------
	 *  Group
	 * ------------------------------------------------------------
	 */
	public static Group createGroup(String name, Group.TYPE type) throws Exception {
		
		//TODO VALIDATION?
		
		Group group = Group.create(name, type);
		group.setStatus(Group.STATUS.ACTIVE);
		
		group.save();
		
		return group;
	}
	
	public static List<User> getMembers(Group group) throws Exception {
		
		List<User> users = new ArrayList<User>();
		
		SelectStatement selectStatement = new SelectStatement(UserRecord.METADATAID, UserRecord.TABLENAME);
		selectStatement.addAllSelectColumns();
		SqlTable userGroupTable = selectStatement.addImportedTable(JOIN_KEY_USER_USERGROUP);
		selectStatement.addWhere(userGroupTable, UserGroupRecord.GROUPID, "=", group.getId());
		
		List<UserRecord> userRecords = UserRecord.listRecords(selectStatement);
		
		for(UserRecord record : userRecords) {
			users.add(new User(record));
		}
		
		return users;
	}
	
	public static UserGroup createUserGroup(User user, Group group, boolean save) throws Exception {
		
		UserGroupRecord record = UserGroupRecord.create(user.getId(), group.getId());
		UserGroup userGroup = new UserGroup(record);
		userGroup.setCanRead(true);
		
		if(save) {
			userGroup.save();
		}
		
		return userGroup;
	}
}