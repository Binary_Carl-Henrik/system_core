package fail.binary.system.core.manager;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.xml.bind.ValidationException;

import fail.binary.system.core.exception.AuthenticationError;
import fail.binary.system.core.exception.UserError;
import fail.binary.system.core.object.Session;
import fail.binary.system.core.object.User;
import fail.binary.system.core.util.EncryptionUtil;

public class AuthenticationManager {
	
	/* ------------------------------------------------------------
	 *  Login
	 * ------------------------------------------------------------
	 */
	/**
	 * Validates the username and password is correct and creates a Session
	 * @param username
	 * @param unencryptedPassword
	 * @return A Session for the user
	 * @throws Exception
	 */
	public static Session login(String username, String unencryptedPassword) throws Exception {
		
		User user = UserManager.getUser(username);
		
		validateUserLogin(user, unencryptedPassword);
		
		Session session = SessionManager.createSession(user);
		
		return session;
	}
	
	private static void validateUserLogin(User user, String unencryptedPassword) throws InvalidKeyException, NoSuchAlgorithmException, ValidationException {
		
		if(user == null) {
			throw new ValidationException(new UserError(AuthenticationError.USER_OR_PASSWORD_INVALID, AuthenticationError.USER_OR_PASSWORD_INVALID_MESSAGE));
		}
		
		String salt = user.getSalt();
		String secret = EncryptionUtil.getSecret(user.getSecretId());
		String passwordHash = EncryptionUtil.generateEncryptedPassword(salt, unencryptedPassword, secret);
		
		if(!passwordHash.equals(user.getEncryptedPassword())) {
			throw new ValidationException(new UserError(AuthenticationError.USER_OR_PASSWORD_INVALID, AuthenticationError.USER_OR_PASSWORD_INVALID_MESSAGE));
		}
	}
}