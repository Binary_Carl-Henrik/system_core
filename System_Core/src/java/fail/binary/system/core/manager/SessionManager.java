package fail.binary.system.core.manager;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

import fail.binary.system.core.object.Session;
import fail.binary.system.core.object.User;
import fail.binary.system.core.storage.Storage;
import fail.binary.system.core.storage.StorageFactory;
import fail.binary.system.core.util.EncryptionUtil;
import fail.binary.system.core.util.StringUtil;

public class SessionManager extends AbstractManager {
	
	/* ------------------------------------------------------------
	 *  Constants & Variables
	 * ------------------------------------------------------------
	 */
	public static final String SECRET_SESSION_ID = "SessionIdSecret";
	public static final String HEADER_API_KEY = "API-KEY";
	
	private static final ConcurrentHashMap<String, Session> _SESSIONS = new ConcurrentHashMap<String, Session>();
	
	/* ------------------------------------------------------------
	 *  Session
	 * ------------------------------------------------------------
	 */
	/**
	 * @param sessionId of the requested session
	 * @return Session of sessionId if found in cache, else null
	 */
	public static Session getSession(String sessionId) {
		
		if(StringUtil.isDefined(sessionId)) {
			return _SESSIONS.get(sessionId);
		}
		
		return null;
	}
	
	/**
	 * @return All cached session IDs
	 */
	public static Iterator<String> getSessionIdIterator() {
		return _SESSIONS.keySet().iterator();
	}
	
	/**
	 * @return All cached sessions
	 */
	public static List<Session> getAllSessions() {
		return new ArrayList<Session>(_SESSIONS.values());
	}
	
	/**
	 * Creates a new Session for the user and adds it to the cache
	 * @param user the session is created for
	 * @return Session
	 * @throws InvalidKeyException
	 * @throws NoSuchAlgorithmException
	 */
	public static synchronized Session createSession(User user) throws InvalidKeyException, NoSuchAlgorithmException {
		
		String secret = EncryptionUtil.getSecret(SECRET_SESSION_ID);
		String creationTime = Long.toString(System.currentTimeMillis()) + user.getId();
		String sessionId = EncryptionUtil.generateEncryptedString(secret, creationTime);
		
		Session session = new Session(sessionId, user);
		
		_SESSIONS.put(sessionId, session);
		
		return _SESSIONS.get(sessionId);
	}
	
	/* ------------------------------------------------------------
	 *  Persistent session
	 * ------------------------------------------------------------
	 */
	/**
	 * 
	 * @param sessionId of the requested session
	 * @return A stored Session of the session ID if found, else null
	 * @throws ClassNotFoundException if the stored Session is of an incorrect version
	 * @throws IOException
	 */
	public static Session getStoredSession(String sessionId) throws ClassNotFoundException, IOException {
		
		Session session = new Session(sessionId, null);
		Storage<Session> storage = StorageFactory.getStorage(session);
		
		Object object = storage.readStoredObject(session);
		
		if(object != null && object instanceof Session) {
			return (Session) object;
		}
		
		return null;
	}
	
	private void storeSession(Session session) {
		Storage<Session> storage = StorageFactory.getStorage(session);
		storage.storeObjectAsync(session);
	}
}