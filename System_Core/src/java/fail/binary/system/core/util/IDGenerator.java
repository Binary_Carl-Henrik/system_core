package fail.binary.system.core.util;

import java.util.concurrent.atomic.AtomicLong;

public class IDGenerator {
	
	/* ------------------------------------------------------------
	 *  Constants & Variables
	 * ------------------------------------------------------------
	 */
	private final static AtomicLong _lastGeneratedId = new AtomicLong(0l);
	
	/* ------------------------------------------------------------
	 *  ID
	 * ------------------------------------------------------------
	 */
	
	/**
	 * @return a unique ID
	 */
	public synchronized final static long generate() {
		
		long currentSystemTime = System.currentTimeMillis();
		
		if(_lastGeneratedId.get() >= currentSystemTime) {
			
			_lastGeneratedId.set(_lastGeneratedId.get() + 1);
		} else {
			_lastGeneratedId.set(currentSystemTime);
		}
		
		return _lastGeneratedId.get();
	}
}