package fail.binary.system.core.util;

//TODO IN XML FILE
public class Config {
	
	/* ------------------------------------------------------------
	 *  Constants & Variables
	 * ------------------------------------------------------------
	 */
	private static final String FILE_STORAGE_LOCATION;
	private static final String SESSION_STORAGE_LOCATION;
	
	static {
		FILE_STORAGE_LOCATION = "D:/MoneyPlanner/Store/";
		SESSION_STORAGE_LOCATION = "Session/";
	}
	
	/* ------------------------------------------------------------
	 *  Storage
	 * ------------------------------------------------------------
	 */
	public static String getFileStorageLocation() {
		return FILE_STORAGE_LOCATION;
	}

	public static String getSessionStorageLocation() {
		return getFileStorageLocation() + SESSION_STORAGE_LOCATION;
	}
}