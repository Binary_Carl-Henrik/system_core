package fail.binary.system.core.util;

import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

public class EncryptionUtil {
	
	/* ------------------------------------------------------------
	 *  Constants & Variables
	 * ------------------------------------------------------------
	 */
	private final static String CRYPTO_HASH_SHA1 = "HmacSHA1";
	
	private final static Integer SALT_LENGTH = 10;
	
	private final static String ALLOWED_CHARACTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvqxyz1234567890@!%&#*-_()$?";
	
	
	@Deprecated
	private final static String TEMP_SECRET = "OnlyTmp!1234@Tmp!";
	
	/* ------------------------------------------------------------
	 *  Encryption
	 * ------------------------------------------------------------
	 */
	/**
	 * Generates an encrypted hash String from the data with the secret
	 * @param secret encryption key
	 * @param data to be encrypted
	 * @return encrypted Base64 encoded String
	 * @throws NoSuchAlgorithmException
	 * @throws InvalidKeyException
	 */
	public static String generateEncryptedString(String secret, String data) throws NoSuchAlgorithmException, InvalidKeyException {
		return new String(Base64.getEncoder().encode(generateEncryptedArray(secret, data)), StandardCharsets.UTF_8);
	}
	
	public static String generateEncryptedPassword(String salt, String password, String secret) throws InvalidKeyException, NoSuchAlgorithmException {
		
		String passwordString = salt + password;
		
		return EncryptionUtil.generateEncryptedString(secret, passwordString);
	}
	
	private static byte[] generateEncryptedArray(String secret, String data) throws NoSuchAlgorithmException, InvalidKeyException {
		
		Mac mac = Mac.getInstance(CRYPTO_HASH_SHA1);
		SecretKeySpec keySpec = new SecretKeySpec(secret.getBytes(), CRYPTO_HASH_SHA1);
		mac.init(keySpec);
		
		return mac.doFinal(data.getBytes(StandardCharsets.UTF_8));
	}
	
	/* ------------------------------------------------------------
	 *  Secret
	 * ------------------------------------------------------------
	 */
	public final static String getSecret(String sercretId) {
		//TODO
		return TEMP_SECRET;
	}
	
	public final static String generateSecret() {
		
		//return generateRandomString(16?);
		//TODO
		return TEMP_SECRET;
	}
	
	/* ------------------------------------------------------------
	 *  Salt
	 * ------------------------------------------------------------
	 */
	/**
	 * @return A random string with 10 characters
	 */
	public final static String generateSalt() {
		return generateRandomString(SALT_LENGTH);
	}
	
	/* ------------------------------------------------------------
	 *  Utilities
	 * ------------------------------------------------------------
	 */
	private final static String generateRandomString(int length) {
		
		StringBuilder randomStringBuilder = new StringBuilder();
		
		for(int i = 0; i<length; i++) {
			int charIndex = (int) (Math.random()*ALLOWED_CHARACTERS.length());
			randomStringBuilder.append(ALLOWED_CHARACTERS.charAt(charIndex));
		}
		
		return randomStringBuilder.toString();
	}
}