package fail.binary.system.core.util;

public class StringUtil {
	
	/**
	 * Checks that the length of the String is longer then the minLength </BR>
	 * null is defined as not valid at all times </BR>
	 * An empty string is valid if the minLength is 0 </BR>
	 * @param value
	 * @param minLength
	 * @return if String is longer or equal to minLength
	 */
	public final static boolean hasLength(String value, int minLength) {
		
		if(minLength == 0 && value != null) {
			return true;
		}
		
		if(isDefined(value)) {
			if(value.length() >= minLength) {
				return true;
			}
		}
		
		return false;
	}
	
	/**
	 * @param value
	 * @return if value is not null and is not empty
	 */
	public final static boolean isDefined(String value) {
		return value != null && !value.isEmpty();
	}
}