package fail.binary.system.core.storage;

import java.io.IOException;

import fail.binary.system.core.exception.SystemException;
import fail.binary.system.core.object.StoredObject;

public interface Storage<T extends StoredObject> {
	
	/**
	 * Stores the T at specified location in the StoredObject
	 * @param object
	 * @return Storage typed in object or null
	 * @throws SystemException
	 * @throws IOException
	 */
	public void storeObject(T object) throws SystemException, IOException;
	
	public void storeObjectAsync(T object);
	
	public Object readStoredObject(T object) throws IOException, ClassNotFoundException;

}
