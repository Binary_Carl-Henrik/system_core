package fail.binary.system.core.storage;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicBoolean;

import fail.binary.system.core.exception.StorageError;
import fail.binary.system.core.exception.SystemException;
import fail.binary.system.core.object.StoredObject;
import fail.binary.system.core.util.Logger;

public class FileStorage<T extends StoredObject> implements Storage<T> {
	
	/* ------------------------------------------------------------
	 *  Constants & Variables
	 * ------------------------------------------------------------
	 */
	public static final String STORAGE_TYPE = FileStorage.class.getSimpleName();
	
	private static final StorageQueue _QUEUE = new StorageQueue();
	
	private final String _location;
	
	
	/* ------------------------------------------------------------
	 *  Initialize
	 * ------------------------------------------------------------
	 */
	public FileStorage(String location) {
		
		if(!location.endsWith("/")) {
			location += "/";
		}
		
		_location = location;
	}
	
	/* ------------------------------------------------------------
	 *  Read
	 * ------------------------------------------------------------
	 */
	public Object readStoredObject(T storedObject) throws IOException, ClassNotFoundException {
		
		File file = new File(_location + storedObject.getStorageId());
		if(!file.exists()) {
			return null;
		}
		
		FileInputStream fileInput = null;
		ObjectInputStream objectInput = null;
		
		try {
			
			fileInput = new FileInputStream(file);
			objectInput = new ObjectInputStream(fileInput);
			
			Object object = objectInput.readObject();
			
			return object;
			
		} finally {
			
			if(objectInput != null) {
				objectInput.close();
			} else if(fileInput != null) {
				fileInput.close();
			}
		}
	}
	
	/* ------------------------------------------------------------
	 *  Store
	 * ------------------------------------------------------------
	 */
	public void storeObject(T object) throws SystemException, IOException {
		writeObjectToFile(object);
	}
	
	public void storeObjectAsync(T object) {
		_QUEUE.addFile(this, object);
	}
	
	private File writeObjectToFile(T object) throws SystemException, IOException {
		
		File folder = new File(_location);
		if(!folder.exists()) {
			if(!folder.mkdirs()) {
				throw generateException(StorageError.COULD_NOT_CREATE_FOLDER, folder);
			}
		}
		
		File file = new File(folder, object.getStorageId());
		if(!file.mkdir()) {
			throw generateException(StorageError.COULD_NOT_CREATE_FILE, file);
		}
		
		FileOutputStream outputStream = null;
		ObjectOutputStream objectStream = null;
		
		try {
			
			outputStream = new FileOutputStream(file);
			objectStream = new ObjectOutputStream(outputStream);
			
			objectStream.writeObject(object);
			
			return file;
			
		} finally {
			if(objectStream != null) {
				objectStream.close();
			} else if(outputStream != null) {
				outputStream.close();
			}
		}
		
	}
	
	/* ------------------------------------------------------------
	 *  Utilities
	 * ------------------------------------------------------------
	 */
	private static class StorageQueue implements Runnable {
		
		/* ------------------------------------------------------------
		 *  Constants & Variables
		 * ------------------------------------------------------------
		 */
		private ConcurrentLinkedQueue<FileStorageExecutor<?>> _queuedFiles;
		private AtomicBoolean _threadIsAlive;
		
		/* ------------------------------------------------------------
		 *  Initialize
		 * ------------------------------------------------------------
		 */
		StorageQueue() {
			_queuedFiles = new ConcurrentLinkedQueue<FileStorageExecutor<?>>();
			_threadIsAlive = new AtomicBoolean(true);
		}
		
		/* ------------------------------------------------------------
		 *  Queue
		 * ------------------------------------------------------------
		 */
		<T extends StoredObject> void addFile(FileStorage<T> storage, T object) {
			
			FileStorageExecutor<T> container = new FileStorageExecutor<T>(storage, object);
			
			_queuedFiles.add(container);
			
			_queuedFiles.notifyAll();
		}
		
		/* ------------------------------------------------------------
		 *  Run
		 * ------------------------------------------------------------
		 */
		@Override
		public void run() {
			
			while(_threadIsAlive.get()) {
				
				if(_queuedFiles.isEmpty()) {
					
					try {
						_queuedFiles.wait();
					} catch (InterruptedException e) {
						Logger.logError(e);
					}
				}
				
				FileStorageExecutor<?> fileStorageContainer = _queuedFiles.poll();
				if(fileStorageContainer != null) {
					
					try {
						
						fileStorageContainer.writeToFile();
						
					} catch (SystemException e) {
						Logger.logError(e);
					} catch (IOException e) {
						Logger.logError(e);
					}
				}
			}
		}
	}
	
	/**
	 * Stores and executes the FileStorage for the local objects
	 * @param <T>
	 */
	private static class FileStorageExecutor<T extends StoredObject> {
		
		/* ------------------------------------------------------------
		 *  Constants & Variables
		 * ------------------------------------------------------------
		 */
		private final FileStorage<T> _fileStorage;
		private final T _storedObject;
		
		/* ------------------------------------------------------------
		 *  Initialize
		 * ------------------------------------------------------------
		 */
		public FileStorageExecutor(FileStorage<T> fileStorage, T storedObject) {
			_fileStorage = fileStorage;
			_storedObject = storedObject;
		}
		
		/* ------------------------------------------------------------
		 *  Execute
		 * ------------------------------------------------------------
		 */
		public void writeToFile() throws SystemException, IOException {
			_fileStorage.writeObjectToFile(_storedObject);
		}
	}
	
	private SystemException generateException(int errorCode, File file) {
		return generateException(new StorageError(errorCode, file));
	}
	
	private SystemException generateException(StorageError error) {
		
		SystemException exception = new SystemException();
		exception.addError(error);
		
		return exception;
	}
}