package fail.binary.system.core.storage;

import fail.binary.system.core.object.StoredObject;

public class StorageFactory {
	
	/**
	 * @param storedObject
	 * @return the Storage defined in the storedObject
	 */
	public static final <T extends StoredObject> Storage<T> getStorage(T storedObject) {
		
		if(FileStorage.STORAGE_TYPE.contentEquals(storedObject.getStorageType())) {
			return new FileStorage<T>(storedObject.getStorageLocation());
		}
		
		return null;
	}
}