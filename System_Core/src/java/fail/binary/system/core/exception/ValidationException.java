package fail.binary.system.core.exception;

import java.util.List;

public class ValidationException extends AbstractException {
	
	/* ------------------------------------------------------------
	 *  Constants & Variables
	 * ------------------------------------------------------------
	 */
	/**
	 * v. 1.0.0
	 */
	private static final long serialVersionUID = -514134147539739655L;
	
	
	/* ------------------------------------------------------------
	 *  Initialize
	 * ------------------------------------------------------------
	 */
	public ValidationException() {
		super();
	}
	
	public ValidationException(List<SystemError> errors) {
		super(errors);
	}

	/* ------------------------------------------------------------
	 *  Error
	 * ------------------------------------------------------------
	 */
	@Override
	public void addError(AbstractError error) {
		errors.add(error);
	}
}