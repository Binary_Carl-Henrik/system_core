package fail.binary.system.core.exception;

public class SystemError extends AbstractError {
	
	/* ------------------------------------------------------------
	 *  Constants & Variables
	 * ------------------------------------------------------------
	 */
	/**
	 * v. 1.0.0
	 */
	private static final long serialVersionUID = 8819380813743789493L;
	
	private final String _exceptionMessage;
	
	
	/* ------------------------------------------------------------
	 *  Initialize
	 * ------------------------------------------------------------
	 */
	public SystemError(int errorCode, String exceptionMessage) {
		super(errorCode);
		_exceptionMessage = exceptionMessage;
	}

	@Override
	public String getErrorMessage() {
		return _exceptionMessage;
	}
}