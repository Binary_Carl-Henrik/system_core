package fail.binary.system.core.exception;

abstract public class AbstractError extends Error {
	
	/* ------------------------------------------------------------
	 *  Constants & Variables
	 * ------------------------------------------------------------
	 */
	/**
	 * v. 1.0.0
	 */
	private static final long serialVersionUID = -131989230581549788L;
	
	private final int _errorCode;
	
	
	/* ------------------------------------------------------------
	 *  Initialize
	 * ------------------------------------------------------------
	 */
	public AbstractError(int errorCode) {
		_errorCode = errorCode;
	}
	
	/* ------------------------------------------------------------
	 *  Error
	 * ------------------------------------------------------------
	 */
	abstract public String getErrorMessage();
	
	public int getErrorCode() {
		return _errorCode;
	}
	
	/* ------------------------------------------------------------
	 *  Message
	 * ------------------------------------------------------------
	 */
	public String toString() {
		return getClass().getSimpleName() + ": " + getErrorCode() + " " + getErrorMessage();
	}
}