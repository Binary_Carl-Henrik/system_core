package fail.binary.system.core.exception;

public class AuthenticationError extends SystemError {

	/* ------------------------------------------------------------
	 *  Constants & Variables
	 * ------------------------------------------------------------
	 */
	public final static int USER_OR_PASSWORD_INVALID = 5110;
	
	//TODO language manage
	public final static String USER_OR_PASSWORD_INVALID_MESSAGE = "The username or the password is invalid";
	
	
	/**
	 * v. 1.0.0
	 */
	private static final long serialVersionUID = 2136578321643311587L;
	
	/* ------------------------------------------------------------
	 *  Initialize
	 * ------------------------------------------------------------
	 */
	public AuthenticationError(int errorCode, String exceptionMessage) {
		super(errorCode, exceptionMessage);
	}
}