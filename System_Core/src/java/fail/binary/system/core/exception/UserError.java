package fail.binary.system.core.exception;

public class UserError extends SystemError {
	
	/* ------------------------------------------------------------
	 *  Constants & Variables
	 * ------------------------------------------------------------
	 */
	public static final int USERNAME_INVALID = 5010;
	public static final int USERNAME_OCCUPIED = 5011;
	
	public static final int PASSWORD_INVALID = 5020;
	
	/**
	 * v. 1.0.0
	 */
	private static final long serialVersionUID = 7232127826442982405L;

	/* ------------------------------------------------------------
	 *  Initialize
	 * ------------------------------------------------------------
	 */
	public UserError(int errorCode, String exceptionMessage) {
		super(errorCode, exceptionMessage);
	}
}