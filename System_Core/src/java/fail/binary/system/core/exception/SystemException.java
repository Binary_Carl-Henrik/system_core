package fail.binary.system.core.exception;

public class SystemException extends AbstractException {

	/* ------------------------------------------------------------
	 *  Constants & Variables
	 * ------------------------------------------------------------
	 */
	/**
	 * v. 1.0.0
	 */
	private static final long serialVersionUID = -7552546511837837152L;
	
	/* ------------------------------------------------------------
	 *  Initialize
	 * ------------------------------------------------------------
	 */
	public SystemException() {
		super();
	}

	/* ------------------------------------------------------------
	 *  Error
	 * ------------------------------------------------------------
	 */
	public void addError(SystemError error) {
		this.addError((AbstractError)error);
	}
	
	public void addError(StorageError error) {
		this.addError((AbstractError)error);
	}
	
	@Override
	protected void addError(AbstractError error) {
		errors.add(error);
	}
}