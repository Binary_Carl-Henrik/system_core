package fail.binary.system.core.exception;

import java.io.File;
import java.net.MalformedURLException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import fail.binary.system.core.util.Logger;

public class StorageError extends AbstractError {

	/* ------------------------------------------------------------
	 *  Constants & Variables
	 * ------------------------------------------------------------
	 */
	public static final int COULD_NOT_CREATE_FOLDER = 1101;
	public static final int COULD_NOT_CREATE_FILE = 1102;
	
	/**
	 * v. 1.0.0
	 */
	private static final long serialVersionUID = -2899997498015442612L;
	
	private static final Map<Integer, String> _EXCEPTION_MESSAGES;
	
	static {
		
		Map<Integer, String> excepctionMessages = new HashMap<Integer, String>();
		excepctionMessages.put(COULD_NOT_CREATE_FOLDER, "Could not create folder");
		excepctionMessages.put(COULD_NOT_CREATE_FILE, "Could not create file");
		
		_EXCEPTION_MESSAGES = Collections.unmodifiableMap(excepctionMessages);
	}
	
	private final File _file;

	/* ------------------------------------------------------------
	 *  Initialize
	 * ------------------------------------------------------------
	 */
	public StorageError(int errorCode, File file) {
		super(errorCode);
		_file = file;
	}
	
	/* ------------------------------------------------------------
	 *  Error
	 * ------------------------------------------------------------
	 */
	public static String getErrorMessage(int errorCode) {
		return _EXCEPTION_MESSAGES.get(errorCode);
	}

	@Override
	public String getErrorMessage() {
		
		String exceptionMessage = getErrorMessage(getErrorCode());
		
		String fileUri = "";
		try {
			fileUri = _file == null ? "N/A" : _file.toURI().toURL().toExternalForm();
		} catch (MalformedURLException e) {
			Logger.logError(e);
		}
		
		return exceptionMessage + ": " + fileUri;
	}
}