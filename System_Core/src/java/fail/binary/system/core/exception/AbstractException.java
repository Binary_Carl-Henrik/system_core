package fail.binary.system.core.exception;

import java.util.ArrayList;
import java.util.List;

abstract public class AbstractException extends Exception {

	/* ------------------------------------------------------------
	 *  Constants & Variables
	 * ------------------------------------------------------------
	 */
	/**
	 * v. 1.0.0
	 */
	private static final long serialVersionUID = 2285259721987099794L;
	
	protected final List<AbstractError> errors;
	
	/* ------------------------------------------------------------
	 *  Initialize
	 * ------------------------------------------------------------
	 */
	public AbstractException() {
		errors = new ArrayList<AbstractError>();
	}
	
	public AbstractException(List<? extends AbstractError> errors) {
		this();
		this.errors.addAll(errors);
	}
	
	/* ------------------------------------------------------------
	 *  Error
	 * ------------------------------------------------------------
	 */
	abstract protected void addError(AbstractError error);
	
	public List<AbstractError> getErrors() {
		return errors;
	}
	
	@Override
	public String getMessage() {
		
		StringBuilder exceptionMessageBuilder = new StringBuilder();
		
		for(AbstractError error : errors) {
			exceptionMessageBuilder.append(error.toString() + "\n");
		}
		
		return exceptionMessageBuilder.toString();
	}
}