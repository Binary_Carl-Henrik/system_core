package fail.binary.system.core.net.access;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;

import fail.binary.system.core.manager.SessionManager;
import fail.binary.system.core.util.StringUtil;

public class APIAccess {
	
	/* ------------------------------------------------------------
	 *  Constants & Variables
	 * ------------------------------------------------------------
	 */
	public final static String HEADER_SESSION_ID = "SessionID";
	
	/* ------------------------------------------------------------
	 *  Session
	 * ------------------------------------------------------------
	 */
	
	/**
	 * @param request
	 * @return If the request contains a SessionID that is cached in the system
	 */
	public static boolean isActiveSession(HttpServletRequest request) {
		
		String sessionId = request.getHeader(HEADER_SESSION_ID);
		return SessionManager.getSession(sessionId) != null;
	}
	
	/**
	 * @param request
	 * @return If the request contains a SessionID that is stored for the system
	 * @throws ClassNotFoundException if the stored session is of an incorrect version
	 * @throws IOException
	 */
	public static boolean isPassiveSession(HttpServletRequest request) throws ClassNotFoundException, IOException {
		
		String sessionId = request.getHeader(HEADER_SESSION_ID);
		return SessionManager.getStoredSession(sessionId) != null;
	}
	
	/**
	 * @param request
	 * @return if the request contains a SessionID that do not exist in the system
	 * @throws IOException 
	 * @throws ClassNotFoundException 
	 */
	public static boolean isBadSession(HttpServletRequest request) throws ClassNotFoundException, IOException {
		
		String sessionId = request.getHeader(HEADER_SESSION_ID);
		return StringUtil.isDefined(sessionId) && !isActiveSession(request) && !isPassiveSession(request);
	}
}