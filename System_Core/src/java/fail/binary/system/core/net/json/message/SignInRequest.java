package fail.binary.system.core.net.json.message;

public class SignInRequest {

	/* ------------------------------------------------------------
	 *  Constants And Variables
	 * ------------------------------------------------------------
	 */
	private String _username;
	private String _password;
	
	/* ------------------------------------------------------------
	 *  Constructor
	 * ------------------------------------------------------------
	 */
	public SignInRequest() {
		
	}
	
	/* ------------------------------------------------------------
	 *  Getters And Setters
	 * ------------------------------------------------------------
	 */
	public void setusername(String username) {
		_username = username;
	}
	
	public String getusername() {
		return _username;
	}
	
	public void setPassword(String password) {
		_password = password;
	}
	
	public String getPassword() {
		return _password;
	}
}