package fail.binary.system.core.net.rest.handler;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import fail.binary.system.core.exception.AbstractError;
import fail.binary.system.core.exception.AbstractException;
import fail.binary.system.core.manager.AuthenticationManager;
import fail.binary.system.core.net.json.message.SignInRequest;
import fail.binary.system.core.net.json.message.SignInResponse;
import fail.binary.system.core.net.json.part.Error;
import fail.binary.system.core.object.Session;

public class AuthenticationRestHandler {
	
	/* ------------------------------------------------------------
	 *  Login
	 * ------------------------------------------------------------
	 */
	public static String login(String body) throws JAXBException, JsonParseException, JsonMappingException, IOException {
		
		//TODO PARTS COULD BE IN GENERIC METHODS
		ObjectMapper objectMapper = new ObjectMapper();
		SignInRequest request = objectMapper.readValue(body, SignInRequest.class);
		SignInResponse response = new SignInResponse();
		
		try {
			
			Session session = AuthenticationManager.login(request.getusername(), request.getPassword());
			response.setSessionId(session.getSessionId());
			
		} catch (Exception e) {
			
			response.setErrors(parseErrors(e));
			
		}
		
		return objectMapper.writeValueAsString(response);
	}
	
	/* ------------------------------------------------------------
	 *  Exception
	 * ------------------------------------------------------------
	 */
	private static List<Error> parseErrors(Exception e) {
		
		//TODO ABSTRACT REST HANDLER?
		
		List<Error> jsonErrors = new ArrayList<Error>();
		
		if(e instanceof AbstractException) {
			
			AbstractException exception = (AbstractException) e;
			List<AbstractError> exceptionErrors = exception.getErrors();
			
			for(AbstractError exceptionError : exceptionErrors) {
				
				Error jsonError = new Error();
				
				jsonError.setErrorCode(exceptionError.getErrorCode());
				jsonError.setErrorMessage(exceptionError.getErrorMessage());
				
				jsonErrors.add(jsonError);
			}
		} else {
			//TODO
		}
		
		return jsonErrors;
	}
}