package fail.binary.system.core.net.json.part;

public class Error {
	
	/* ------------------------------------------------------------
	 *  Constants & Variables
	 * ------------------------------------------------------------
	 */
	private int _errorCode;
	private String _errorMessage;
	
	/* ------------------------------------------------------------
	 *  Constructor
	 * ------------------------------------------------------------
	 */
	public Error() {
		
	}
	
	/* ------------------------------------------------------------
	 *  Getters And Setters
	 * ------------------------------------------------------------
	 */
	public void setErrorCode(int errorCode) {
		_errorCode = errorCode;
	}
	
	public int getErrorCode() {
		return _errorCode;
	}
	
	public void setErrorMessage(String errorMessage) {
		_errorMessage = errorMessage;
	}
	
	public String getErrorMessage() {
		return _errorMessage;
	}
}