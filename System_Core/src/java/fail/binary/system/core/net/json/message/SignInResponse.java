package fail.binary.system.core.net.json.message;

import fail.binary.system.core.net.json.part.Error;
import java.util.List;

public class SignInResponse {
	
	/* ------------------------------------------------------------
	 *  Constants & Variables
	 * ------------------------------------------------------------
	 */
	private String _sessionId;
	private List<Error> _errors;
	
	/* ------------------------------------------------------------
	 *  Getters and Setters
	 * ------------------------------------------------------------
	 */
	public void setSessionId(String sessionId) {
		_sessionId = sessionId;
	}
	
	public String getSessionId() {
		return _sessionId;
	}
	
	public List<Error> getErrors() {
		return _errors;
	}
	
	public void setErrors(List<Error> errors) {
		_errors = errors;
	}
}