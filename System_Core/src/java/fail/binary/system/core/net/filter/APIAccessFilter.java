package fail.binary.system.core.net.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fail.binary.system.core.net.access.APIAccess;
import fail.binary.system.core.util.Logger;

public class APIAccessFilter implements Filter {

	@Override
	public void destroy() { }

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		
		HttpServletRequest httpRequest = (HttpServletRequest)request;
		
		try {
			
			if(APIAccess.isActiveSession(httpRequest) || APIAccess.isPassiveSession(httpRequest)) {
				
				((HttpServletResponse)response).setStatus(HttpServletResponse.SC_ACCEPTED);
				chain.doFilter(httpRequest, response);
				
			}  else if(APIAccess.isBadSession(httpRequest)) {
				
				((HttpServletResponse)response).setStatus(HttpServletResponse.SC_UNAUTHORIZED);
				
			} else {
				
				((HttpServletResponse)response).setStatus(HttpServletResponse.SC_BAD_REQUEST);
				
			}
			
		} catch (ClassNotFoundException e) {
			
			((HttpServletResponse)response).setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			
			Logger.logError(e);
		}
	}

	@Override
	public void init(FilterConfig arg0) throws ServletException { }
}