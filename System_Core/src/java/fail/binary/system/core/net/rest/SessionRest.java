package fail.binary.system.core.net.rest;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.stream.Collectors;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.JAXBException;

import fail.binary.system.core.net.rest.handler.AuthenticationRestHandler;
import fail.binary.system.core.util.Logger;

public class SessionRest extends HttpServlet  {
	
	/**
	 * v. 1.0.0
	 */
	private static final long serialVersionUID = 5488438075511681680L;
	
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String body = request.getReader().lines().collect(Collectors.joining(System.lineSeparator()));
		try {
			
			String responseBody = AuthenticationRestHandler.login(body);
			
			response.setStatus(HttpServletResponse.SC_OK);
			PrintWriter writer = response.getWriter();
			writer.write(responseBody);
			writer.flush();
			writer.close();
			
		} catch (JAXBException e) {
			
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			
			Logger.logError(e);
		}
	}
}